#### Description ####
Polymer element combining functionality of:
- google-map
- google-map-search
- google-map-marker

#### Usage ####
<dh-google-map query="New York best pizza"></dh-google-map>

The result will be a google map centered on marker. Position of the marker will be determined by running value of
query argument against Google Maps API.

#### Dependencies ####
As defined in bower.json:
- GoogleWebComponents/google-map

#### Installation ####
Depend on this package in your bower.json file:
`"dh-google-map": "git@bitbucket.org:develhell/dh-google-map.git#~0.0.6"`